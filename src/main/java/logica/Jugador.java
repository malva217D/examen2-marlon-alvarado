package logica;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import javax.swing.JOptionPane;

public class Jugador extends Usuario {

    //Atributos
    private String usuario;
    private String fecha_ingreso;
    private Nave[][] tablero = new Nave[5][5];
    private List<String> naves_disponibles = Arrays.asList(
            "destructor", "fragata", "submarino", "explorador", "portaviones"
    );

    //Constructor
    public Jugador() {
        super();
    }

    public Jugador(String usuario, String fecha_ingreso, int codigo, String nombre, String nacionalidad) {
        super(codigo, nombre, nacionalidad);
        this.usuario = usuario;
        this.fecha_ingreso = fecha_ingreso;
    }

    public Jugador(String usuario, String fecha_ingreso) {
        this.usuario = usuario;
        this.fecha_ingreso = fecha_ingreso;
    }

    //Metodos 
    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getFecha_ingreso() {
        return fecha_ingreso;
    }

    public void setFecha_ingreso(String fecha_ingreso) {
        this.fecha_ingreso = fecha_ingreso;
    }

    public Nave[][] getTablero() {
        return tablero;
    }

    public void setTablero(Nave[][] tablero) {
        this.tablero = tablero;
    }

    public List<String> getNaves_disponibles() {
        return naves_disponibles;
    }

    public void setNaves_disponibles(List<String> naves_disponibles) {
        this.naves_disponibles = naves_disponibles;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    // Metodo para Crear Jugadores
    public Jugador LlenarDatos() {
        Random r = new Random(4);
        this.codigo = r.nextInt(10000) + 1;
        this.nombre = JOptionPane.showInputDialog("Ingrese su Nombre: ");
        this.nacionalidad = JOptionPane.showInputDialog("Ingrese su Nacionalidad: ");
        this.fecha_ingreso= JOptionPane.showInputDialog("Ingrese la fecha Ingreso(dd/mm/yyyy): ");
        this.usuario = JOptionPane.showInputDialog("Ingrese su Correo(Usuario@utn.ac.cr): ");
        while (!this.usuario.contains("@utn.ac.cr")) {
            JOptionPane.showMessageDialog(null, "Error, solo cuentas UTN se puede registrar");
            this.usuario = JOptionPane.showInputDialog("Ingrese su Correo: ");
        }
        return this;
    }
    
    // llenar Tablero
    public void llenarTablero(String modo) {
        switch (modo) {
            case "automaticamente":
                this.llenarAutomaticamente();
                break;
            case "manualmente":
                this.llenarManualmente();
                break;
            default:
                break;
        }
    }
  
    //Metodo tablero Automatico
    public void llenarAutomaticamente() {
        int columna = 0;
        int fila = 0;
        int opcion = 0;
        String clase = "";
        String municion = "";
        Nave nave;
        for (int i = 0; i < 10; i++) {
            do {
                columna = (int) (Math.random() * 5);
                fila = (int) (Math.random() * 5);
            } while (this.tablero[fila][columna] != null);
            opcion = (int) (Math.random() * 5);
            clase = naves_disponibles.get(opcion);
            municion = this.obtenerMunicion(clase);
            nave = new Nave(0, true, "0", 0.0, clase, municion, 1000);
            tablero[fila][columna] = nave;
        }
    }

    //Metodo Tablero Manual
    public void llenarManualmente() {
        Nave matriz[][] = null;
        int nFilas;
        int nCol;
        Nave nave = null;

        nFilas = Integer.parseInt(JOptionPane.showInputDialog("Ingrese la Cantidad de Filas: "));

        while (nFilas < 5) {
            JOptionPane.showMessageDialog(null, "Min 5 filas");
            nFilas = Integer.parseInt(JOptionPane.showInputDialog("Ingrese la Cantidad de Filas: "));
        }

        nCol = Integer.parseInt(JOptionPane.showInputDialog("Ingrese la Cantidad de Columnas: "));

        while (nCol < 5) {
            JOptionPane.showMessageDialog(null, "Min 5 columnas");
            nCol = Integer.parseInt(JOptionPane.showInputDialog("Ingrese la Cantidad de Columnas: "));
        }

        matriz = new Nave[nFilas][nCol];
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz.length; j++) {
                if (matriz[i][j] == null) {
                    nave = new Nave(10, true, "D-1", 100, "destructor", "balas", 1000);
                    tablero[i][j] = nave;
                }
                
            }
            
            
        }
        this.setTablero(matriz);
        JOptionPane.showInternalMessageDialog(null, "Tablero creado con Exito");

    }

    //Metodo para Obtner Munciones
    public String obtenerMunicion(String clase) {
        String municion = "";
        switch (clase) {
            case "destructor":
                municion = "bala";
                break;
            case "fragata":
                municion = "misil";
                break;
            case "submarino":
                municion = "misil";
                break;
            case "explorador":
                municion = "bomba";
                break;
            case "portaviones":
                municion = "bala";
                break;
            default:
                municion = "bala";
                break;
        }
        return municion;
    }

    //Metodo de imprimir Tablero
    public void imprimirTablero() {
        String tablero_lleno = "";
        for (int i = 0; i < 5; i++) {
            tablero_lleno += "\n";
            for (int j = 0; j < 5; j++) {
                if (this.tablero[i][j] == null) {
                    tablero_lleno += " ___________ |";
                } else {
                    tablero_lleno += this.obtenerString(this.tablero[i][j].getClase());
                }
            }
        }
        System.out.println("------Tablero del Jugador: "+ this.getNombre()+ "----------"
                +tablero_lleno+ "\n");
    }

    //Metodo para Obtener los Navios
    public String obtenerString(String clase) {
        String string = "";
        switch (clase) {
            case "destructor":
                string = " destructor  |";
                break;
            case "fragata":
                string = " fragata     |";
                break;
            case "submarino":
                string = " submarino   |";
                break;
            case "explorador":
                string = " explorador  |";
                break;
            case "portaviones":
                string = " portaviones |";
                break;
            default:
                string = " ___________ |";
                break;
        }
        return string;
    }
}
