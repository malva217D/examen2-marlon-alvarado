package logica;

public class Nave extends Navio {

    private int marineros;
    private boolean estado;
    private String simbologia;
    private double daño_acumulado;

    public Nave(int marineros, boolean estado, String simbologia, double daño_acumulado, String clase, String tipo_municion, int litros_combustible) {
        super(clase, tipo_municion, litros_combustible);
        this.marineros = marineros;
        this.estado = estado;
        this.simbologia = simbologia;
        this.daño_acumulado = daño_acumulado;
    }

    public int getMarineros() {
        return marineros;
    }

    public void setMarineros(int marineros) {
        this.marineros = marineros;
    }

    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public String getSimbologia() {
        return simbologia;
    }

    public void setSimbologia(String simbologia) {
        this.simbologia = simbologia;
    }

    public double getDaño_acumulado() {
        return daño_acumulado;
    }

    public void setDaño_acumulado(float daño_acumulado) {
        this.daño_acumulado = daño_acumulado;
    }

    public String getClase() {
        return clase;
    }

    public void setClase(String clase) {
        this.clase = clase;
    }

    public String getTipo_municion() {
        return tipo_municion;
    }

    public void setTipo_municion(String tipo_municion) {
        this.tipo_municion = tipo_municion;
    }

    public int getLitros_combustible() {
        return litros_combustible;
    }

    public void setLitros_combustible(int litros_combustible) {
        this.litros_combustible = litros_combustible;
    }
    
    public void disparar(){
        
    }
}
