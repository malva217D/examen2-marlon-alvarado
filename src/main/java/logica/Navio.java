package logica;

public abstract class Navio implements InterfazNavio{
    
    protected String clase, tipo_municion;
    protected int litros_combustible;

    public Navio(String clase, String tipo_municion, int litros_combustible) {
        this.clase = clase;
        this.tipo_municion = tipo_municion;
        this.litros_combustible = litros_combustible;
    }

    public String getClase() {
        return clase;
    }

    public void setClase(String clase) {
        this.clase = clase;
    }

    public String getTipo_municion() {
        return tipo_municion;
    }

    public void setTipo_municion(String tipo_municion) {
        this.tipo_municion = tipo_municion;
    }

    public int getLitros_combustible() {
        return litros_combustible;
    }

    public void setLitros_combustible(int litros_combustible) {
        this.litros_combustible = litros_combustible;
    }
}
