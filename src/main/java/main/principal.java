package main;

import java.util.ArrayList;
import javax.swing.JOptionPane;
import logica.Jugador;

public class principal {

    public static void main(String[] args) {

        ArrayList<Jugador> jugadores = new ArrayList<Jugador>();
        int menuOpciones;
        int agregarMas;
        boolean continuar = true;

        do {
            menuOpciones = Integer.parseInt(JOptionPane.showInputDialog("Bienvenido a Batalla Naval \n"
                    + "1-Jugar\n"
                    + "2-Salir"));

            switch (menuOpciones) {
                case 1: //
                    do {
                        Jugador j = new Jugador();
                        j.LlenarDatos();
                        Object[] modo = {"Tablero Automatico", "Tablero Manual"};
                        String opcModo = "Tablero Automatico";
                        Object opcFinal = JOptionPane.showInputDialog(null, "MODO TABLERO", "Opción", JOptionPane.QUESTION_MESSAGE, null, modo, opcModo);
                        String OpcionRealizar1 = opcFinal.toString();

                        if ("Tablero Automatico".equals(OpcionRealizar1)) {
                            j.llenarTablero("automaticamente");
                            j.imprimirTablero();

                        } else if ("Tablero Manual".equals(OpcionRealizar1)) {
                            j.llenarTablero("manualmente");
                            j.imprimirTablero();
                        }
                        jugadores.add(j);
                        agregarMas = JOptionPane.showConfirmDialog(null, "Desea agregar otro jugador?");

                    } while (agregarMas == 0);

                    //COMIENZA EL JUGO
                    JOptionPane.showMessageDialog(null, "COMIENZA EL JUEGO");

                    break;

                case 2:
                    continuar = false;
                    break;

                default:
                    break;

            }

        } while (continuar);

    }

}
